//
// Created by miti on 2020-01-01.
//

#ifndef DECRYPTORAPP_MAINLOGIC_H
#define DECRYPTORAPP_MAINLOGIC_H
#include <unistd.h>
#include "LA.h"
#include "ProtobufMessageRW.h"
#include <string>
#include <vector>
#include "PostLAMessaging.h"

class MainLogic {
    uint32_t enclave_id;
    PostLAMessaging messaging;
    LA laInitiator;
    int set_up_listening_socket(int port);
    int accept_fd(int fd);
    int la_post_la_with_verifier();
    int la_post_la_with_target();
public:
    void set_enclave_id(uint32_t given_enclave_id);
    int run_deployment_runtime_stage_logic();
};


#endif //DECRYPTORAPP_MAINLOGIC_H

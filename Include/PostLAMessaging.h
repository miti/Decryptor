//
// Created by miti on 2020-01-01.
//

#ifndef DECRYPTORAPP_POSTLAMESSAGING_H
#define DECRYPTORAPP_POSTLAMESSAGING_H

#include "ProtobufMessageRW.h"
#include "PostLAMessages.pb.h"
#include <string>
#include <vector>
class PostLAMessaging {
    uint32_t enclave_id;
    int verifier_fd;
    ProtobufMessageRW targetChannelData;
    ProtobufMessageRW targetChannelHeaders;
    int process_target_data_msg(extension_to_decryptor_msg& incomingMsg, decryptor_to_extension_msg& outgoingMsg);
public:
    int read_and_write_header();
    int receive_token_from_verifier();
    int receive_decrypt_client_data_from_target();
    void set_enclave_id(int given_id);
    void set_verifier_fd(int fd);
    void set_target_data_fd(int fd);
    void set_target_headers_fd(int fd);
};


#endif //DECRYPTORAPP_POSTLAMESSAGING_H

#ifndef ECDHKEYPAIR_H
#define ECDHKEYPAIR_H
#include "sgx_tcrypto.h" 
  static const int ECDH_PUBLIC_KEY_SIZE =2*SGX_ECP256_KEY_SIZE;
  static const int ECDH_PRIVATE_KEY_SIZE =SGX_ECP256_KEY_SIZE;
class ECDHKeypair {
private:  
  uint8_t private_key[ECDH_PUBLIC_KEY_SIZE];
  uint8_t public_key[ECDH_PUBLIC_KEY_SIZE];
public:
  void set_private_public_key(uint8_t* ip_private_key, uint8_t* ip_public_key); 
  void get_private_key(uint8_t* op_private_key); 
  uint32_t generate_keypair();
 
//	ECDHKeypair(); 
	void get_public_key(uint8_t* op_public_key); 
//	~ECDHKeypair(); 
};
#endif

//
// Created by miti on 2019-12-21.
//

#ifndef DECRYPTORAPP_SEALING_H
#define DECRYPTORAPP_SEALING_H
#include "Decryptor_u.h"
// for sealing - sgx_calc_sealed_data_size
#include "sgx_tseal.h"
// For reading from/writing to file -sealing.

#include<unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
class Sealing {
    uint32_t write_to_fd(int fd, uint8_t* msg, uint32_t* expected_msg_length);
    uint32_t read_from_fd(int fd, uint8_t* msg, uint32_t* expected_msg_length);
    uint32_t unseal_signing_key_pair_from_disk(sgx_enclave_id_t enclave_id, int fd, size_t sealed_msg_length_in_file);
    uint32_t create_and_seal_signing_key_pair_to_disk(sgx_enclave_id_t enclave_id, int fd);
public:
    uint32_t initialize_long_term_keypair(sgx_enclave_id_t enclave_id, const char* filename);
};

#endif //DECRYPTORAPP_SEALING_H

#include "ECDHKeypair.h"
#include "SymmetricEncryptionBox.h"
class HybridEncryptionBox : public ECDHKeypair, public SymmetricEncryptionBox {
  public:
    unsigned long initialize_symmetric_key(uint8_t* given_public_key); 
//    HybridEncryptionBox(); 
//    ~HybridEncryptionBox(); 
};

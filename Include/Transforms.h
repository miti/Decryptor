//
// Created by miti on 2020-01-01.
//

#ifndef DECRYPTORAPP_TRANSFORMS_H
#define DECRYPTORAPP_TRANSFORMS_H

#include "sgx_eid.h"
#include "error_codes.h"
#include "datatypes.h"
#include "sgx_urts.h"
#include "sgx_dh.h"

// For msg1
#define SGX_TARGET_INFO_RESERVED1_BYTES 4
#define SGX_TARGET_INFO_RESERVED2_BYTES 456
#define SGX_ECP256_KEY_SIZE 32
#define SGX_HASH_SIZE 32 /* SHA256 */
// For msg2
#define SGX_REPORT_DATA_SIZE 64
#define SGX_KEYID_SIZE 32
#define SGX_DH_MAC_SIZE 16
#define SGX_REPORT_BODY_RESERVED1 28
#define SGX_REPORT_BODY_RESERVED2 32
#define SGX_REPORT_BODY_RESERVED3 96
#define SGX_REPORT_BODY_RESERVED4 60


#include <stdio.h>
// For google protobufs and deserialization/serialization
#include "ProtobufLAMessages.pb.h"
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
using namespace google::protobuf::io;
#include <inttypes.h>


class Transforms {
    static int fit_32_into_uint8_t(google::protobuf::uint32 temp32, uint8_t* temp8);
    static int fit_32_into_uint16_t(google::protobuf::uint32 temp32, uint16_t* temp16);
    static void  encode_ec256_public_key_to_protobuf(protobuf_sgx_ec256_public_t* protobuf_g_a , sgx_ec256_public_t* g_a);
    static int decode_ec256_public_key_from_protobuf(protobuf_sgx_ec256_public_t* protobuf_g_a , sgx_ec256_public_t* g_a);
    static void  encode_attributes_to_protobuf(protobuf_sgx_attributes_t* protobuf_attributes, sgx_attributes_t* attributes);
    static void  encode_report_to_protobuf(protobuf_sgx_report_t* protobuf_report, sgx_report_t* report);
    static int decode_attributes_from_protobuf(protobuf_sgx_attributes_t* protobuf_attributes, sgx_attributes_t* attributes);
    static int decode_report_from_protobuf(protobuf_sgx_report_t* protobuf_report, sgx_report_t* report);
public:
    static void encode_msg1_to_protobuf( protobuf_sgx_dh_msg1_t& protobuf_dhmsg1, sgx_dh_msg1_t* native_dhmsg1);
    static void encode_msg3_to_protobuf(protobuf_sgx_dh_msg3_t& protobuf_dhmsg3, sgx_dh_msg3_t* native_dhmsg3);
    static int decode_msg2_from_protobuf(protobuf_sgx_dh_msg2_t& protobuf_dhmsg2, sgx_dh_msg2_t* native_dhmsg2);
    static int print_initialized_msg1( protobuf_sgx_dh_msg1_t& protobuf_dhmsg1);
};

#endif //DECRYPTORAPP_TRANSFORMS_H

//
// Created by miti on 2020-01-01.
//

#ifndef DECRYPTORAPP_LA_H
#define DECRYPTORAPP_LA_H

#include "ProtobufMessageRW.h"
#include "ProtobufLAMessages.pb.h"

class LA {
    uint32_t enclave_id;
    ProtobufMessageRW protobufReaderWriter;
    uint32_t process_protobuf_dh_msg2_generate_protobuf_dh_msg3( protobuf_sgx_dh_msg2_t& protobuf_msg2, protobuf_sgx_dh_msg3_t& protobuf_msg3);
    uint32_t generate_protobuf_dh_msg1( protobuf_sgx_dh_msg1_t& protobuf_msg1);
public:
    uint32_t conduct_la(int fd);
    void set_enclave_id(uint32_t given_eid);
};

#endif //DECRYPTORAPP_LA_H

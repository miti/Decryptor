#include "ECDHKeypair.h"
#include <stdint.h>
class ECDSASignatureBox : public ECDHKeypair {
public:
  void get_keypair(uint8_t* output_keypair);
  uint32_t sign(uint8_t* signature_data, uint32_t signature_data_length, uint8_t* signature);
//  ECDSASignatureBox(); 
//  ~ECDSASignatureBox();
  };

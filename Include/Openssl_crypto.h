#include<stdint.h>
#include<string.h>
int ecdh_key_gen(unsigned char* pub_key_x, unsigned char* pub_key_y, unsigned char* priv_key);
unsigned long check_ecdh_public_key(unsigned char* given_key_x, unsigned char* given_key_y);
unsigned long compute_ecdh_shared_key(unsigned char* given_key_x, unsigned char* given_key_y, unsigned char* priv_key, unsigned char* derived_key);
int generate_sha256_hash(const unsigned char *message, size_t message_len, unsigned char *digest);
int aes_gcm_128(int enc, unsigned char *key, unsigned char *iv, unsigned char* plaintext, uint32_t plaintext_len, unsigned char *ciphertext,  uint32_t* op_ciphertext_len, uint8_t* tag);
int base64_decoding_wrapper(unsigned char* src, unsigned char* dest, uint32_t length);
int compute_ecdsa_signature(unsigned char* signature_data, uint32_t signature_data_length, unsigned char* own_private_key, unsigned char* signature); 

#ifndef SYMMETRICENCRYPTIONBOX_H
#define SYMMETRICENCRYPTIONBOX_H
#include "Openssl_crypto.h"
class SymmetricEncryptionBox {
  uint8_t symmetric_key[16]; // SHA256 used with ECDHE -> or AES whatever symmetric_key.
  public:
    void set_symmetric_key(uint8_t* given_key);
    void get_symmetric_key(uint8_t* op_key); 
    uint32_t encrypt_decrypt(int enc, uint8_t* plaintext, uint32_t plaintext_length, uint8_t* ciphertext, uint32_t* ciphertext_length);
};
#endif

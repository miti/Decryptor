#
# Copyright (C) 2011-2017 Intel Corporation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in
#     the documentation and/or other materials provided with the
#     distribution.
#   * Neither the name of Intel Corporation nor the names of its
#     contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#

######## SGX SDK Settings ########

SGX_MODE := HW
SGX_ARCH := x64
SGX_DEBUG := 1

ifeq ($(shell getconf LONG_BIT), 32)
	SGX_ARCH := x86
else ifeq ($(findstring -m32, $(CXXFLAGS)), -m32)
	SGX_ARCH := x86
endif

SGX_COMMON_CFLAGS := -m64
SGX_LIBRARY_PATH := $(SGX_SDK)/lib64
SGX_ENCLAVE_SIGNER := $(SGX_SDK)/bin/x64/sgx_sign
SGX_EDGER8R := $(SGX_SDK)/bin/x64/sgx_edger8r


SGX_COMMON_CFLAGS += -O2

######## Library Settings ########
#Integrating linux-sgx-ssl (adapted from the makefile for the TestApp in that repo)
SGXSSL_PATH := /opt/intel/sgxssl/
SGXSSL_LIBRARY_PATH := $(SGXSSL_PATH)/lib64
SGXSSL_Crypto_Library := sgx_tsgxssl_crypto
SGXSSL_SSL_Library := sgx_tsgxssl
SgxSSL_Link_Libraries := -L$(SGXSSL_LIBRARY_PATH) -Wl,--whole-archive -l$(SGXSSL_SSL_Library) -Wl,--no-whole-archive -l$(SGXSSL_Crypto_Library)
Security_Link_Flags := -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now -pie
SGXSSL_INCLUDE_PATH := $(SGXSSL_PATH)/include

Trust_Lib_Name := libLocalAttestation_Trusted.a
TrustLib_Include_Paths := -I$(SGX_SDK)/include -I$(SGX_SDK)/include/tlibc -I$(SGX_SDK)/include/libcxx -I$(SGX_SDK)/include/epid -I./Include -I./Decryptor
TrustLib_Compile_Flags := $(SGX_COMMON_CFLAGS) -nostdinc -fvisibility=hidden -fpie -fstack-protector  $(TrustLib_Include_Paths) -Wall
TrustLib_Compile_Cxx_Flags :=  -std=c++11 -nostdinc++

######## App Settings ########

Urts_Library_Name := sgx_urts

App_Cpp_Files := $(wildcard App/*.cpp)
App_Include_Paths := -I$(SGX_SDK)/include -I$(SGX_SDK)/include/ippcp -I./Include -I./LocalAttestationCode
App_Compile_Flags := $(SGX_COMMON_CFLAGS) -fPIC -Wno-attributes $(App_Include_Paths) -std=c++11 -Wall -DDEBUG -UNDEBUG -UEDEBUG
App_Link_Flags := $(SGX_COMMON_CFLAGS) $(Security_Link_Flags) -L$(SGX_LIBRARY_PATH) -l$(Urts_Library_Name) -lsgx_uae_service -L$(SGXSSL_LIBRARY_PATH) -lsgx_usgxssl -lpthread  -lprotobuf -lsgx_uae_service

App_Cpp_Objects := App/App.o App/LocalAttestationUntrusted.o App/protobufLAInitiator.o App/protobufSgxLATransformsInitiator.o App/ProtobufLAMessages.pb.o #$(App_Cpp_Files:.cpp=.o)
App_Name := app

######## Enclave Settings ########

Decryptor_Version_Script := Decryptor/Decryptor.lds

Trts_Library_Name := sgx_trts
Service_Library_Name := sgx_tservice
Crypto_Library_Name := sgx_tcrypto

Enclave_Cpp_Files_2 := $(wildcard Decryptor/*.cpp)
Enclave_Include_Paths := -I$(SGX_SDK)/include -I$(SGX_SDK)/include/tlibc -I$(SGX_SDK)/include/libcxx -I./LocalAttestationCode -I./Include

CC_BELOW_4_9 := $(shell expr "`$(CC) -dumpversion`" \< "4.9")
ifeq ($(CC_BELOW_4_9), 1)
	Enclave_Compile_Flags := $(SGX_COMMON_CFLAGS) -nostdinc -fvisibility=hidden -fpie -ffunction-sections -fdata-sections -fstack-protector -Wno-unused-variable -Wall
else
	Enclave_Compile_Flags := $(SGX_COMMON_CFLAGS) -nostdinc -fvisibility=hidden -fpie -ffunction-sections -fdata-sections -fstack-protector-strong -Wno-unused-variable -Wall
endif

Enclave_Compile_Flags += $(Enclave_Include_Paths)

# To generate a proper enclave, it is recommended to follow below guideline to link the trusted libraries:
#    1. Link sgx_trts with the `--whole-archive' and `--no-whole-archive' options,
#       so that the whole content of trts is included in the enclave.
#    2. For other libraries, you just need to pull the required symbols.
#       Use `--start-group' and `--end-group' to link these libraries.
# Do NOT move the libraries linked with `--start-group' and `--end-group' within `--whole-archive' and `--no-whole-archive' options.
# Otherwise, you may get some undesirable errors.
Common_Enclave_Link_Flags := $(SGX_COMMON_CFLAGS) -Wl,--no-undefined -nostdlib -nodefaultlibs -nostartfiles \
    $(SgxSSL_Link_Libraries) $(Security_Link_Flags) \
	-Wl,--whole-archive -l$(Trts_Library_Name) -Wl,--no-whole-archive \
	-Wl,--start-group -L$(SGX_LIBRARY_PATH) -L. -L./Decryptor -L./LocalAttestationCode -lsgx_tstdc -lsgx_tcxx -l$(Crypto_Library_Name) -l$(Service_Library_Name) -Wl,--end-group \
	-Wl,-Bstatic -Wl,-Bsymbolic  \
	-Wl,-pie,-eenclave_entry -Wl,--export-dynamic \
	-Wl,--defsym,__ImageBase=0 -Wl,--gc-sections
Decryptor_Link_Flags := $(Common_Enclave_Link_Flags) -Wl,--version-script=$(Decryptor_Version_Script)
Enclave_Cpp_Objects_2 := $(Enclave_Cpp_Files_2:.cpp=.o)
Enclave_Name_2 := libDecryptor.so

all: .config_$(Build_Mode)_$(SGX_ARCH) $(Trust_Lib_Name) $(UnTrustLib_Name) $(Enclave_Name_2) $(App_Name)

.config_$(Build_Mode)_$(SGX_ARCH):
	@rm -rf .config_* $(App_Name) *.so *.a App/*.o Decryptor/*.o Decryptor/*_t.* Decryptor/*_u.* LocalAttestationCode/*.o LocalAttestationCode/*_t.* #Enclave3/*.o Enclave3/*_t.* Enclave3/*_u.*
	@touch .config_$(Build_Mode)_$(SGX_ARCH)

######## Library Objects ########

LocalAttestationCode/LocalAttestationCode_t.c LocalAttestationCode/LocalAttestationCode_t.h : $(SGX_EDGER8R) LocalAttestationCode/LocalAttestationCode.edl
	@cd LocalAttestationCode && $(SGX_EDGER8R) --trusted ../LocalAttestationCode/LocalAttestationCode.edl --search-path $(SGX_SDK)/include
	@echo "GEN  =>  $@"

LocalAttestationCode/LocalAttestationCode_t.o: LocalAttestationCode/LocalAttestationCode_t.c
	@$(CC) $(TrustLib_Compile_Flags) -c $< -o $@
	@echo "CC   <=  $<"

LocalAttestationCode/LocalAttestationTrustedWrapper.o: LocalAttestationCode/LocalAttestationTrustedWrapper.cpp LocalAttestationCode/LocalAttestationCode_t.h
	@$(CXX) $(TrustLib_Compile_Flags) $(TrustLib_Compile_Cxx_Flags) -c $< -o $@
	@echo "CC   <= $<"

LocalAttestationCode/LocalAttestationTrusted.o: LocalAttestationCode/LocalAttestationTrusted.cpp LocalAttestationCode/LocalAttestationCode_t.h
	@$(CXX) $(TrustLib_Compile_Flags) $(TrustLib_Compile_Cxx_Flags) -c $< -o $@
	@echo "CC   <= $<"

$(Trust_Lib_Name): LocalAttestationCode/LocalAttestationCode_t.o LocalAttestationCode/LocalAttestationTrustedWrapper.o LocalAttestationCode/LocalAttestationTrusted.o
	@$(AR) rcs $@ $^
	@echo "GEN  =>  $@"

CommonOpensslCode/Openssl_crypto.o: CommonOpensslCode/Openssl_crypto.cpp
	@$(CXX) -std=c++11 -nostdinc++ $(Enclave_Compile_Flags) -v -I$(SGXSSL_INCLUDE_PATH)  -c $< -o $@
	@echo "CXX  <=  $<"

######## App Objects ########
Include/Decryptor_u.h: $(SGX_EDGER8R) Decryptor/Decryptor.edl
	@cd Decryptor && $(SGX_EDGER8R) --use-prefix --untrusted ../Decryptor/Decryptor.edl --search-path $(SGX_SDK)/include --untrusted-dir ../Include/
	@echo "GEN  =>  $@"

App/Decryptor_u.c: $(SGX_EDGER8R) Decryptor/Decryptor.edl Include/Decryptor_u.h
	@cd App && $(SGX_EDGER8R) --use-prefix --untrusted ../Decryptor/Decryptor.edl --search-path $(SGX_SDK)/include
	@echo "GEN  =>  $@"

App/Decryptor_u.o: App/Decryptor_u.c
	@$(CC) $(App_Compile_Flags) -c $< -o $@
	@echo "CC   <=  $<"

App/%.o: App/%.cpp Include/Decryptor_u.h
	@$(CXX) $(App_Compile_Flags) -c $< -o $@
	@echo "CXX  <=  $<"

$(App_Name): App/App.o App/Sealing.o App/MainLogic.o App/PostLAMessaging.o App/LAResponder/LA.o App/LAResponder/Transforms.o  App/PostLAMessaging.o App/PostLAMessages.pb.o App/ProtobufLAMessages.pb.o  App/ProtobufMessageRW.o App/Decryptor_u.o
	@$(CXX) -Wl,--no-undefined $^ -o $@ $(App_Link_Flags)
	@echo "LINK =>  $@"

######### Crypto classes for enclave ############


######## Enclave Objects ########
Decryptor/Decryptor_t.c: $(SGX_EDGER8R) Decryptor/Decryptor.edl
	@cd Decryptor && $(SGX_EDGER8R)  --use-prefix --trusted ../Decryptor/Decryptor.edl --search-path $(SGX_SDK)/include
	@echo "GEN  =>  $@"

Decryptor/Decryptor_t.o: Decryptor/Decryptor_t.c
	@$(CC) $(Enclave_Compile_Flags) -c $< -o $@
	@echo "CC   <=  $<"

Decryptor/%Box.o: Decryptor/%Box.cpp 
	@$(CXX) -std=c++11 -nostdinc++ $(Enclave_Compile_Flags)  -c $< -o $@
	@echo "CXX  <=  $<"

Decryptor/ECDHKeypair.o: Decryptor/ECDHKeypair.cpp
	@$(CXX) -std=c++11 -nostdinc++ $(Enclave_Compile_Flags)  -c $< -o $@
	@echo "CXX   <=  $<"

Decryptor/DecryptorWrapper.o: Decryptor/DecryptorWrapper.cpp
	@$(CXX) -std=c++11 -nostdinc++ $(Enclave_Compile_Flags)  -c $< -o $@
	@echo "CXX  <=  $<"

Decryptor/Decryptor.o: Decryptor/Decryptor.cpp
	@$(CXX) -std=c++11 -nostdinc++ $(Enclave_Compile_Flags)  -c $< -o $@
	@echo "CXX  <=  $<"

Decryptor.so: Decryptor/Decryptor_t.o Decryptor/DecryptorWrapper.o Decryptor/Decryptor.o $(Trust_Lib_Name) Decryptor/HybridEncryptionBox.o Decryptor/ECDSASignatureBox.o Decryptor/SymmetricEncryptionBox.o Decryptor/ECDHKeypair.o CommonOpensslCode/Openssl_crypto.o 
	@$(CXX) $^ -o $@ $(Decryptor_Link_Flags)
	@echo "LINK =>  $@"

$(Enclave_Name_2): Decryptor.so
	@$(SGX_ENCLAVE_SIGNER) sign -key Decryptor/Decryptor_private.pem -enclave Decryptor.so -out $@ -config Decryptor/Decryptor.config.xml
	@echo "SIGN =>  $@"

######## Clean ########
.PHONY: clean

clean:
	@rm -rf .config_* $(App_Name) *.so *.a App/*.o App/LAResponder/*.o Decryptor/*.o Decryptor/*_t.* App/Decryptor_u.c Include/Decryptor_u.h LocalAttestationCode/*.o LocalAttestationCode/*_t.* CommonOpensslCode/*.o

//#include <map>
#include "ECDSASignatureBox.h"
#include "HybridEncryptionBox.h"
#include "LocalAttestationTrusted.h"

class Decryptor {
  static ECDSASignatureBox signatureBox;
  static HybridEncryptionBox hybridEncryptionBoxClient;
  static uint8_t verifier_mr_enclave[32]; // =  {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  static SymmetricEncryptionBox symmetricEncryptionBoxApache;
  static SymmetricEncryptionBox symmetricEncryptionBoxVerifier;
  static uint8_t apache_mr_signer[32]; // = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; 
  static uint8_t plaintext_mitigator_header_H[ECDH_PUBLIC_KEY_SIZE + 32 + 64];
  static uint8_t first_decryption_output[1092]; // 1000 bytes of ciphertext data + 12 IV + 16 Tag + 64 clients public key
  static uint8_t plaintext_client_data[1000]; 


  static uint32_t create_mitigator_token_M(uint8_t* token);
  static uint32_t create_mitigator_header_H(uint8_t* signature_data_and_signature);
  static uint32_t create_long_term_signing_keypair(uint8_t* private_public_key_string);
  static uint32_t initialize_symmetric_key_decrypt_client_data(uint8_t* plaintext_client_public_key_plus_encrypted_data_plus_tag, uint32_t total_length, uint8_t* plaintext_client_data, uint32_t* plaintext_client_data_length);
    static uint32_t encrypt_decrypt_fields_list(SymmetricEncryptionBox &symmetricEncryptionBox, uint32_t encrypt_decrypt, uint8_t *input_fields_list,
                                                uint32_t *input_field_sizes, uint32_t no_of_fields,
                                                uint8_t *output_fields_list, uint32_t *output_field_sizes, uint32_t* total_output_length);

public:
    static void calculate_sealed_keypair_size(uint32_t* output_length); 
    static uint32_t verify_peer_enclave_trust(uint8_t* given_mr_enclave, uint8_t* given_mr_signer, uint8_t* dhaek);
    static uint32_t create_and_seal_long_term_signing_key_pair(uint32_t* sealed_data_length, uint8_t* sealed_data);
    static uint32_t create_and_encrypt_mitigator_header_H(uint8_t* ciphertext_token_H_plus_tag, uint32_t* length);
    static uint32_t unseal_and_restore_long_term_signing_key_pair(uint8_t* sealed_data, uint32_t* sgx_sealed_data_length);
    static uint32_t decrypt_verifiers_message_set_apache_mrsigner(uint8_t* ciphertext_plus_tag);
    static uint32_t process_apache_message_generate_response(uint8_t* input_ciphertext, uint32_t input_ciphertext_plus_tag_length, uint8_t* output_ciphertext, uint32_t* output_ciphertext_plus_tag_length);
    static uint32_t decrypt_client_data(uint8_t *input_fields_list, uint32_t *input_field_sizes,
                                        uint32_t no_of_fields, uint32_t total_input_size,
                                        uint8_t *output_fields_list,
                                        uint32_t *output_field_sizes);
    static uint32_t process_verifiers_message(uint8_t* input_ciphertext, uint32_t length);
	static  void testing_get_verifier_mrenclave_apache_mrsigner(uint8_t* output); 
	static void testing_get_short_term_public_key(uint8_t* output); 
  static void testing_long_term_verification_key(uint8_t* output); 
	static void testing_get_apache_iv(uint8_t*); 
  };

//ECDSASignatureBox Decryptor::signatureBox(); 
//HybridEncryptionBox Decryptor::hybridEncryptionBoxClient(); 
//SymmetricEncryptionBox Decryptor::symmetricEncryptionBoxApache(); 
//SymmetricEncryptionBox Decryptor::symmetricEncryptionBoxVerifier();
//uint8_t Decryptor::verifier_mr_enclave = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; 
//uint8_t Decryptor::apache_mr_signer = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};




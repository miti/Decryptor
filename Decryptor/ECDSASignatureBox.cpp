#include "Openssl_crypto.h"
#include <stdint.h>
#include "ECDSASignatureBox.h"
  uint32_t ECDSASignatureBox::sign(uint8_t* signature_data, uint32_t signature_data_length, uint8_t* signature)
  {
    uint8_t private_key[ECDH_PRIVATE_KEY_SIZE]; 
	get_private_key(private_key); 
    return compute_ecdsa_signature(signature_data, signature_data_length, private_key, signature);
  }
  void ECDSASignatureBox::get_keypair(uint8_t* output_keypair)
  {
    get_private_key(output_keypair); 
    get_public_key(output_keypair + ECDH_PRIVATE_KEY_SIZE); 
  }

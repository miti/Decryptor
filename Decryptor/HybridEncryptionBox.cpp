#include <stdint.h>
#include "HybridEncryptionBox.h"

unsigned int long HybridEncryptionBox::initialize_symmetric_key(uint8_t* given_public_key)
    {
	uint8_t symmetric_key[32]; // underlying crypto was set to return 32 bytes. just trimming it to return 16 instead (so that the same symmetricencryptiobox primitive works for both encryption between enclaves and between decryptor and client 
	uint8_t private_key[32]; 
	get_private_key(private_key); 
	unsigned long ret_value= compute_ecdh_shared_key(given_public_key, given_public_key + ECDH_PUBLIC_KEY_SIZE/2, private_key, symmetric_key);
	if(ret_value==0)
	{
		set_symmetric_key(symmetric_key); // JUST SETS 1ST 16 BYTES. 
	}
	return ret_value;
    }


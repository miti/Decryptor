#include "Decryptor.h"
#include "sgx_tseal.h"
#include "sgx_tcrypto.h"
#include "sgx_dh.h"
#include "datatypes.h"
#include "error_codes.h"


        ECDSASignatureBox Decryptor::signatureBox;
        HybridEncryptionBox Decryptor::hybridEncryptionBoxClient;
        SymmetricEncryptionBox Decryptor::symmetricEncryptionBoxApache;
        SymmetricEncryptionBox Decryptor::symmetricEncryptionBoxVerifier;
        uint8_t Decryptor::verifier_mr_enclave[32] = {0};
        uint8_t Decryptor::apache_mr_signer[32] = {0};
        unsigned int successful_la_count;
        uint8_t Decryptor::plaintext_mitigator_header_H[ECDH_PUBLIC_KEY_SIZE + 32 + 64] = {0};
        uint8_t Decryptor::first_decryption_output[1092] = {0};
        uint8_t Decryptor::plaintext_client_data[1000] = {0};


  // INTERNAL
  uint32_t Decryptor::create_mitigator_token_M(uint8_t* token)
  {
    uint32_t internal_return_status;
    uint32_t counter;
    // create short-term ECDH key pair
    internal_return_status = hybridEncryptionBoxClient.generate_keypair();
    if(internal_return_status != 0)
      return internal_return_status;
    hybridEncryptionBoxClient.get_public_key(token);

    // create token: concatenate short-term keypair with verifiers mrenclave.
    for(counter=0;counter<32;counter++)
      *(token + counter + ECDH_PUBLIC_KEY_SIZE) = verifier_mr_enclave[counter];

    return 0;
  }

  // INTERNAL
  uint32_t Decryptor::create_mitigator_header_H(uint8_t* signature_data_and_signature)
  {
    uint32_t internal_return_status;
    uint8_t local_signature_data_and_signature[ECDH_PUBLIC_KEY_SIZE + 32 + 64];
    uint32_t counter;
    internal_return_status = Decryptor::create_mitigator_token_M(local_signature_data_and_signature);
    if(internal_return_status != 0x0)
      return internal_return_status;

    internal_return_status = signatureBox.sign(local_signature_data_and_signature, ECDH_PUBLIC_KEY_SIZE + 32, local_signature_data_and_signature + ECDH_PUBLIC_KEY_SIZE + 32);
    if(internal_return_status != 0x0)
      return internal_return_status;
    for(counter=0;counter<ECDH_PUBLIC_KEY_SIZE + 32 + 64;counter++)
      signature_data_and_signature[counter] = local_signature_data_and_signature[counter];
    return 0;
  }

  uint32_t Decryptor::create_and_encrypt_mitigator_header_H(uint8_t* ciphertext_token_H_plus_tag, uint32_t* op_length)
  {
      uint32_t temp_ciphertext_token_H_iv_tag_length;
      uint32_t internal_return_status;
      uint8_t plaintext_mitigator_header_H[ECDH_PUBLIC_KEY_SIZE + 32 + 64] = {0x42};

      if(successful_la_count != 2)
        return 0x33;

      internal_return_status = create_mitigator_header_H(plaintext_mitigator_header_H);
      if(internal_return_status != 0)
        return internal_return_status;

      internal_return_status = symmetricEncryptionBoxApache.encrypt_decrypt(1, plaintext_mitigator_header_H, ECDH_PUBLIC_KEY_SIZE + 32 + 64, ciphertext_token_H_plus_tag, &temp_ciphertext_token_H_iv_tag_length);
      *op_length = temp_ciphertext_token_H_iv_tag_length;
      return internal_return_status;
    }

  // INTERNAL. done. But there might be one more return statement for the case when get_keypair returns sth (it is non void).
  uint32_t Decryptor::create_long_term_signing_keypair(uint8_t* private_public_key_string)
  {
    uint32_t internal_return_status;
    internal_return_status = signatureBox.generate_keypair();
    if(internal_return_status != 0)
      return internal_return_status;

    signatureBox.get_keypair(private_public_key_string);
    return 0;
  }


  // INTERNAL.
  // TODO: Edit to process a list of fields.
  uint32_t Decryptor::initialize_symmetric_key_decrypt_client_data(uint8_t* plaintext_client_public_key_plus_encrypted_data_plus_tag, uint32_t total_length, uint8_t* plaintext_client_data, uint32_t* plaintext_client_data_length)
  {
    uint8_t* ciphertext_plus_tag;
    uint32_t ciphertext_plus_tag_length;
    uint32_t internal_return_status;

    // I will derive a shared key from the plaintext_client_public_key
    internal_return_status = hybridEncryptionBoxClient.initialize_symmetric_key(plaintext_client_public_key_plus_encrypted_data_plus_tag);
    if(internal_return_status != 0)
      return internal_return_status;

    // and then I will decrypt the rest of the client data with that key.
    ciphertext_plus_tag = plaintext_client_public_key_plus_encrypted_data_plus_tag + ECDH_PUBLIC_KEY_SIZE;
    ciphertext_plus_tag_length = total_length - ECDH_PUBLIC_KEY_SIZE;

    internal_return_status = hybridEncryptionBoxClient.encrypt_decrypt(0, ciphertext_plus_tag, ciphertext_plus_tag_length, plaintext_client_data, plaintext_client_data_length);
    return internal_return_status;
  }

  void Decryptor::testing_long_term_verification_key(uint8_t* output)
  {
	uint8_t keypair[ECDH_PUBLIC_KEY_SIZE + ECDH_PRIVATE_KEY_SIZE];
	uint32_t counter; 
	signatureBox.get_keypair(keypair); 
	for(counter=0;counter<ECDH_PUBLIC_KEY_SIZE; counter++)
		output[counter]=keypair[ECDH_PRIVATE_KEY_SIZE+counter]; 
  }

  // EXTERNAL. DONE.
  uint32_t Decryptor::create_and_seal_long_term_signing_key_pair(uint32_t* sealed_data_length, uint8_t* sealed_data)
  {
        uint32_t sgx_libcall_status;
        uint32_t internal_return_status;
        uint32_t temp_sealed_data_length;
        uint8_t* temp_sealed_data;
        uint8_t private_public_key_string[ECDH_PUBLIC_KEY_SIZE + ECDH_PRIVATE_KEY_SIZE];
        uint32_t counter;

        temp_sealed_data_length = sgx_calc_sealed_data_size(0, ECDH_PUBLIC_KEY_SIZE + ECDH_PRIVATE_KEY_SIZE);
        if(temp_sealed_data_length == 0xFFFFFFFF)
          return 0x01;


        temp_sealed_data = (uint8_t*) malloc(temp_sealed_data_length);

        internal_return_status = create_long_term_signing_keypair(private_public_key_string);
      	if(internal_return_status != 0)
      	{
      		free(temp_sealed_data);
      		return internal_return_status;
      	}

        sgx_libcall_status = sgx_seal_data(0, NULL, 3*SGX_ECP256_KEY_SIZE, private_public_key_string, temp_sealed_data_length, (sgx_sealed_data_t*) temp_sealed_data);
        if(sgx_libcall_status != SGX_SUCCESS)
        {
          free(temp_sealed_data);
          return sgx_libcall_status;
        }

        for(counter=0;counter<temp_sealed_data_length;counter++)
    			*(sealed_data + counter)= *(temp_sealed_data + counter);
	      *sealed_data_length = temp_sealed_data_length;
        free(temp_sealed_data);

        return 0;
    }

  // EXTERNAL. DONE.
  uint32_t Decryptor::unseal_and_restore_long_term_signing_key_pair(uint8_t* sealed_data, uint32_t* sgx_sealed_data_length)
  {
      uint32_t temp_plaintext_length;
      uint8_t* temp_plaintext;
      uint32_t counter;
      uint32_t ret_status;
      uint8_t* temp_sealed_data ;

      temp_sealed_data = (uint8_t*) malloc(*sgx_sealed_data_length);
      for(counter=0;counter<*sgx_sealed_data_length;counter++)
        *(temp_sealed_data+counter)=*(sealed_data+counter);

      temp_plaintext_length = sgx_get_encrypt_txt_len((sgx_sealed_data_t*)sealed_data);
      if(temp_plaintext_length == 0xffffffff)
        return 0xFFFFFFFF;
      temp_plaintext = (uint8_t*)malloc( temp_plaintext_length );

      ret_status = sgx_unseal_data((sgx_sealed_data_t*)temp_sealed_data, NULL, 0, temp_plaintext, &temp_plaintext_length);
      free(temp_sealed_data);
      if(ret_status != SGX_SUCCESS)
      {
        free(temp_plaintext);
        return ret_status;
      }

      signatureBox.set_private_public_key(temp_plaintext, temp_plaintext + ECDH_PRIVATE_KEY_SIZE);
      free(temp_plaintext);
      return 0;
    }

  uint32_t Decryptor::process_verifiers_message(uint8_t* input_ciphertext_plus_tag, uint32_t length)
  {
        uint8_t first_decryption_output[150];
        uint32_t first_decryption_output_length;
        uint32_t  internal_return_status;
        uint32_t counter;
        if(successful_la_count != 1) // else, the untrusted application can call this on the first message by apache and cause the verifier to set its mrsigner.
          return 0x23;

        internal_return_status = symmetricEncryptionBoxVerifier.encrypt_decrypt(0, input_ciphertext_plus_tag, length, first_decryption_output, &first_decryption_output_length);
        if(internal_return_status != 0)
	  return internal_return_status;
	
        if(first_decryption_output_length != 32) 
          return 0x33;
        
        for(counter=0; counter<32; counter++)
          apache_mr_signer[counter] = *(first_decryption_output + counter);
        
        
        return 0;
  }

    uint32_t Decryptor::encrypt_decrypt_fields_list(SymmetricEncryptionBox &symmetricEncryptionBox , uint32_t encrypt_decrypt,
            uint8_t *input_fields_list, uint32_t *input_field_sizes,
                                                    uint32_t no_of_fields,
                                                    uint8_t *output_fields_list,
                                                    uint32_t *output_field_sizes, uint32_t* total_output_length) {
      // Enc-dec loops over no_of_fields.
      uint32_t field_counter, input_byte_accumulator=0, byte_counter, field_size, output_byte_accumulator=0;
      uint8_t* input_field, *output_field;
      uint32_t input_field_length, output_field_length, internal_return_status, counter;

      output_field = output_fields_list;
      for(field_counter=0; field_counter< no_of_fields; field_counter++)
      {
          input_field_length = input_field_sizes[field_counter];
          input_field = input_fields_list + input_byte_accumulator;
          output_field = output_fields_list + output_byte_accumulator;

          internal_return_status = symmetricEncryptionBox.encrypt_decrypt(encrypt_decrypt, input_field, input_field_length, output_field, &output_field_length);
          if(internal_return_status != 0)
              return internal_return_status;

          output_field_sizes[field_counter] = output_field_length;
          input_byte_accumulator += input_field_length;
          output_byte_accumulator += output_field_length;
      }
      *total_output_length = output_byte_accumulator;
      return 0;
  }

  uint32_t Decryptor::decrypt_client_data(uint8_t *input_fields_list, uint32_t *input_field_sizes,
                                          uint32_t no_of_fields, uint32_t total_input_size,
                                          uint8_t *output_fields_list,
                                          uint32_t *output_field_sizes) {

      uint8_t *intermediate_fields_list, *plaintext_fields_list, *temp_output_list;
      uint32_t *intermediate_fields_sizes, *plaintext_fields_sizes, *temp_output_sizes;
      uint32_t internal_return_status, total_intermediate_fields_length;
      uint32_t total_plaintext_fields_length, total_output_fields_length, counter;
      // List of symmetric decryption outputs - will get rid of 12 bytes of IV and 16 bytes of tag.
      intermediate_fields_list = (uint8_t*) malloc(total_input_size ); // - (28 * no_of_fields)
      intermediate_fields_sizes = (uint32_t*) malloc(no_of_fields);

      // Remove outermost layer of decryption, namely the one with the target enclave.
      internal_return_status = encrypt_decrypt_fields_list(symmetricEncryptionBoxApache, 0, input_fields_list,
              input_field_sizes, no_of_fields,
              intermediate_fields_list, intermediate_fields_sizes, &total_intermediate_fields_length);
      if(internal_return_status != 0)
          return internal_return_status;

      // Establish a symmetric encryption key with the client, using the client's public key.
      // TODO: Change this function to take in the key length.
      internal_return_status = hybridEncryptionBoxClient.initialize_symmetric_key(intermediate_fields_list);
      if(internal_return_status != 0)
          return internal_return_status;

      // The public key, which was the first element of the output list above, has been processed.
      // Do not attempt to decrypt it one more time.
      intermediate_fields_list += intermediate_fields_sizes[0];
      intermediate_fields_sizes += 1;
      no_of_fields -= 1;
      total_intermediate_fields_length -= intermediate_fields_sizes[0];

      // Initializing lists for the plaintext data.
      plaintext_fields_list = (uint8_t*) malloc(total_intermediate_fields_length);
      plaintext_fields_sizes = (uint32_t*) malloc(no_of_fields);

      // Obtain plaintext data by decrypting with the above newly-established key with the client.
      internal_return_status = encrypt_decrypt_fields_list(hybridEncryptionBoxClient, 0,
              intermediate_fields_list, intermediate_fields_sizes, no_of_fields,
              plaintext_fields_list, plaintext_fields_sizes, &total_plaintext_fields_length);
      if(internal_return_status != 0)
          return internal_return_status;

      temp_output_list = (uint8_t*) malloc(total_input_size);
      temp_output_sizes = (uint32_t*) malloc(no_of_fields);
      // Finally, encrypt the plaintext back to the target enclave.
      internal_return_status = encrypt_decrypt_fields_list(symmetricEncryptionBoxApache, 1, plaintext_fields_list, plaintext_fields_sizes,
               no_of_fields, temp_output_list, temp_output_sizes, &total_output_fields_length);
      if(internal_return_status != 0)
          return internal_return_status;

      for(counter=0; counter<total_output_fields_length; counter++)
          output_fields_list[counter] = temp_output_list[counter];
      for(counter=0;counter<no_of_fields; counter++)
          output_field_sizes[counter] = temp_output_sizes[counter];

      return 0;
  }

uint32_t Decryptor::process_apache_message_generate_response(uint8_t* input_ciphertext, uint32_t input_ciphertext_plus_tag_length, uint8_t* output_ciphertext_plus_tag, uint32_t* output_ciphertext_plus_tag_length)
  {
      uint32_t first_decryption_output_length, plaintext_client_data_length;
      uint32_t internal_return_status;
      // TODO: May be have temporary variables for input ciphertext as they can't be passed directly to functions?
      // first, I decrypt the message from the target enclave, to get the client's public key and ciphertext data (and tag and IV) 
      internal_return_status = symmetricEncryptionBoxApache.encrypt_decrypt(0, input_ciphertext, input_ciphertext_plus_tag_length, first_decryption_output, &first_decryption_output_length);
      if(internal_return_status != 0)
      	return internal_return_status;
	
	// then I obtain the plaintext client data, using the client's public key and own key to ultimately decrypt the client's ciphertext data
      internal_return_status = initialize_symmetric_key_decrypt_client_data(first_decryption_output, first_decryption_output_length, plaintext_client_data, &plaintext_client_data_length);
      if(internal_return_status != 0)
        return internal_return_status;

      // then I will encrypt the plaintext data to the target enclave.
      internal_return_status = symmetricEncryptionBoxApache.encrypt_decrypt(1, plaintext_client_data, plaintext_client_data_length, output_ciphertext_plus_tag, output_ciphertext_plus_tag_length); 
	return internal_return_status; 
    }

  // INTERNAL.
  uint32_t Decryptor::verify_peer_enclave_trust(uint8_t* given_mr_enclave, uint8_t* given_mr_signer, uint8_t* dhaek)
  {
        uint32_t count;
        uint32_t internal_return_status;
        if(successful_la_count == 0) // verifier enclave
        {
                for(count=0; count<SGX_HASH_SIZE; count++)
                        verifier_mr_enclave[count] = given_mr_enclave[count];
		            symmetricEncryptionBoxVerifier.set_symmetric_key(dhaek);
        }
        else // apache enclave
        {
                /*
                for(count=0; count<SGX_HASH_SIZE; count++)
                {
                        if( given_mr_signer[count] != apache_mr_signer[count] )
                                return ENCLAVE_TRUST_ERROR;
                }
                */
		            symmetricEncryptionBoxApache.set_symmetric_key(dhaek);
        }
        successful_la_count ++;
        return SGX_SUCCESS;
  }

  void Decryptor::calculate_sealed_keypair_size(uint32_t* output_length)
  {
	*output_length = sgx_calc_sealed_data_size(0, ECDH_PUBLIC_KEY_SIZE + ECDH_PRIVATE_KEY_SIZE);
}


	void Decryptor::testing_get_verifier_mrenclave_apache_mrsigner(uint8_t* output)
	{
		uint32_t counter;
		for(counter=0; counter<32;counter++)
		{
			output[counter]=verifier_mr_enclave[counter];
			output[counter+32]=apache_mr_signer[counter];
		}
	}

	void Decryptor::testing_get_short_term_public_key(uint8_t* output)
	{
		hybridEncryptionBoxClient.get_public_key(output); 
	}


void Decryptor::testing_get_apache_iv(uint8_t* op)
{
	//	symmetricEncryptionBoxApache.get_iv(op);
}

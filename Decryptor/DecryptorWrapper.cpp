#include <stdint.h>
#include "Decryptor.h"
#include "Decryptor_t.h"

uint32_t process_apache_message_generate_response_wrapper(uint8_t* input_ciphertext, uint32_t input_ciphertext_plus_tag_length, uint8_t* output_ciphertext, uint32_t* output_ciphertext_plus_tag_length)
{
  return Decryptor::process_apache_message_generate_response(input_ciphertext, input_ciphertext_plus_tag_length, output_ciphertext, output_ciphertext_plus_tag_length);
}

uint32_t process_verifiers_message_wrapper(uint8_t* input_ciphertext, uint32_t length)
{
  return Decryptor::process_verifiers_message(input_ciphertext, length);
}

uint32_t create_and_encrypt_mitigator_header_H_wrapper(uint8_t* ciphertext_token_H_plus_tag, uint32_t* length)
{
  return Decryptor::create_and_encrypt_mitigator_header_H(ciphertext_token_H_plus_tag, length);
}

void calculate_sealed_keypair_size_wrapper(uint32_t* length)
{
  Decryptor::calculate_sealed_keypair_size(length);
}

uint32_t create_and_seal_long_term_signing_key_pair_wrapper(uint32_t* sealed_data_length, uint8_t* sealed_data)
{
     return Decryptor::create_and_seal_long_term_signing_key_pair(sealed_data_length, sealed_data);
}

uint32_t unseal_and_restore_long_term_signing_key_pair_wrapper(uint8_t* sealed_data, uint32_t* sgx_sealed_data_length)
{
  return Decryptor::unseal_and_restore_long_term_signing_key_pair(sealed_data, sgx_sealed_data_length);
}

void get_verifier_mrenclave_apache_mrsigner_wrapper(uint8_t* output)
{
	Decryptor::testing_get_verifier_mrenclave_apache_mrsigner(output);
}

void get_short_term_public_key_wrapper(uint8_t* output)
{
	Decryptor::testing_get_short_term_public_key(output);
}

void get_long_term_verification_key_wrapper(uint8_t* output)
{
	Decryptor::testing_long_term_verification_key(output);
}

void get_apache_iv(uint8_t* op)
{
	Decryptor::testing_get_apache_iv(op);
}

uint32_t decrypt_client_data_wrapper(uint8_t *input_fields_list, uint32_t *input_field_sizes,
                                        uint32_t no_of_fields, uint32_t total_input_size,
                                        uint8_t *output_fields_list,
                                        uint32_t *output_field_sizes)
{
    return Decryptor::decrypt_client_data(input_fields_list, input_field_sizes,
            no_of_fields, total_input_size,
            output_fields_list, output_field_sizes);
    }

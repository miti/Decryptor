#include "Openssl_crypto.h"
#include "SymmetricEncryptionBox.h"
#include "sgx_trts.h"
  // increments last 4 bytes (in big-endian order)
/*    uint32_t SymmetricEncryptionBox::recursive_incrementer(uint32_t counter, uint32_t min_counter) 
{
	if(min_counter>counter)
		return 0xfe;
	if(iv[counter] != 0xff)
	{
		iv[counter]+=1; 
		return 0; 
	}
	else
	{
		iv[counter]=0;
		if(counter!=min_counter)
			// counter-1 refers to the overall counter in the iv string (counter+1 for little endian)
			return recursive_incrementer(counter-1,min_counter); 
		else
			return 0xff; 
	}
}

 uint32_t SymmetricEncryptionBox::aes_gcm_increment_iv()  {
        return recursive_incrementer(11, 8); 
  }
*/
  void SymmetricEncryptionBox::set_symmetric_key(uint8_t* given_key)
  {
    uint32_t counter;
    for(counter=0; counter<16; counter++)
      symmetric_key[counter] = given_key[counter];
  }

    void SymmetricEncryptionBox::get_symmetric_key(uint8_t* op_key)
    {
	    uint32_t counter;
	    for(counter=0; counter<16; counter++)
	      op_key[counter] = symmetric_key[counter];
    }

  // TODO: IMPORTANT - CHECK IF WE NEED TO DO THE AES GCM INCREMENT IV FOR DECRYPTION TOO.
  uint32_t SymmetricEncryptionBox::encrypt_decrypt(int enc, uint8_t* plaintext, uint32_t plaintext_length, uint8_t* ciphertext, uint32_t* ciphertext_length)
  {
    uint32_t actual_plaintext_length=plaintext_length; 
    uint8_t tag[16];uint32_t counter, return_status;
	uint8_t iv[12]; 
    if(enc == 0) // decryption
	{
                for(counter=0;counter<16;counter++)
                        tag[counter]=plaintext[counter+plaintext_length-16]; 
		for(counter=0;counter<12;counter++)
			iv[counter]=plaintext[counter+plaintext_length-28]; 
		actual_plaintext_length-=28; 
	}
	else // encryption
	{
		return_status=sgx_read_rand(iv, 12); 
		if(return_status != 0)
			return return_status;
	}
    return_status = aes_gcm_128(enc, symmetric_key, iv, plaintext, actual_plaintext_length, ciphertext, ciphertext_length, tag);
	if(enc == 1 && return_status ==0)
	{
                for(counter=0;counter<12;counter++)
                        ciphertext[counter + *ciphertext_length] = iv[counter];
		for(counter=0;counter<16;counter++)
			ciphertext[counter + 12 + *ciphertext_length] = tag[counter];
		*ciphertext_length=*ciphertext_length + 28;
	}
    return return_status;
  }

/*  void SymmetricEncryptionBox::get_iv(uint8_t* op_iv)
  {
	uint32_t counter;
	for(counter=0;counter<12;counter++)
		op_iv[counter]=iv[counter];
  }
*/

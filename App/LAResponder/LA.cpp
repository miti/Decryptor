//
// Created by miti on 2020-01-01.
//
#include "LA.h"
#include <stdio.h>
#include "Decryptor_u.h"
#include "sgx_eid.h"
#include "sgx_urts.h"
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include "ProtobufLAMessages.pb.h"
#include "Transforms.h"

uint32_t LA::generate_protobuf_dh_msg1(protobuf_sgx_dh_msg1_t& protobuf_msg1)
{
    sgx_dh_msg1_t dh_msg1;            //Diffie-Hellman Message 1
    memset(&dh_msg1, 0, sizeof(sgx_dh_msg1_t));
    uint32_t ret_status;
    Decryptor_session_request_wrapper(enclave_id, &ret_status, &dh_msg1);
    if(ret_status != SGX_SUCCESS)
        return 0xffffffff;

    Transforms::encode_msg1_to_protobuf(protobuf_msg1, &dh_msg1);
    printf("Generated the following Msg1: ------------- \n"); fflush(stdout);
    Transforms::print_initialized_msg1(protobuf_msg1);
    return 0;
}

uint32_t LA::process_protobuf_dh_msg2_generate_protobuf_dh_msg3(protobuf_sgx_dh_msg2_t& protobuf_msg2, protobuf_sgx_dh_msg3_t& protobuf_msg3)
{
    uint32_t ret_status;
    sgx_dh_msg2_t dh_msg2;            //Diffie-Hellman Message 2
    sgx_dh_msg3_t dh_msg3;            //Diffie-Hellman Message 3
    sgx_key_128bit_t dh_aek;        // Session Key
    memset(&dh_aek,0, sizeof(sgx_key_128bit_t));
    memset(&dh_msg2, 0, sizeof(sgx_dh_msg2_t));
    memset(&dh_msg3, 0, sizeof(sgx_dh_msg3_t));

    if(Transforms::decode_msg2_from_protobuf(protobuf_msg2, &dh_msg2)!=0)
        return -1;

    // process msg2 and generate msg3
    Decryptor_exchange_report_wrapper(enclave_id, &ret_status, &dh_msg2, &dh_msg3);
    if(ret_status!=SGX_SUCCESS)
        return 0x35;

    // convert msg3 sgx_dh_msg3_t object to a protobuf msg3 object.
    Transforms::encode_msg3_to_protobuf(protobuf_msg3, &dh_msg3);
    return 0;
}

uint32_t LA::conduct_la(int fd)
{
    // declare msg1, msg2, msg3 protobuf objects
    protobuf_sgx_dh_msg1_t protobuf_msg1;
    protobuf_sgx_dh_msg2_t protobuf_msg2;
    protobuf_sgx_dh_msg3_t protobuf_msg3;
    uint32_t protobuf_sgx_ret;

    setbuf(stdout,NULL);

    protobufReaderWriter.set_fd(fd);

    protobuf_sgx_ret = generate_protobuf_dh_msg1(protobuf_msg1);
    if(protobuf_sgx_ret != 0)
    {
        printf("Error in generate_protobuf_dh_msg1: 0x%x", protobuf_sgx_ret); fflush(stdout); return protobuf_sgx_ret;
    }

    printf("Writing message 1\n"); fflush(stdout);
    if(protobufReaderWriter.write_msg(protobuf_msg1)!=0)
        return 0x1;

    printf("Reading message 2\n"); fflush(stdout);
    if(protobufReaderWriter.read_msg(protobuf_msg2)!=0)
        return 0x2;

    protobuf_sgx_ret = process_protobuf_dh_msg2_generate_protobuf_dh_msg3(protobuf_msg2, protobuf_msg3);
    if(protobuf_sgx_ret != 0)
    {
        printf("Error in process_protobuf_dh_msg2_generate_protobuf_dh_msg3: 0x%x", protobuf_sgx_ret);
        fflush(stdout);
        return protobuf_sgx_ret;
    }

    printf("Writing message 3\n"); fflush(stdout);
    if(protobufReaderWriter.write_msg(protobuf_msg3)!=0)
        return 0x3;
    return 0;

}

void LA::set_enclave_id(uint32_t given_eid)
{
    enclave_id = given_eid;
}

// App.cpp : Defines the entry point for the console application.
#include <stdio.h>
#include <map>
#include "sgx_eid.h"
#include "sgx_urts.h"
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
// #include "LocalAttestationUntrusted.h"
#include "Sealing.h"
#include "MainLogic.h"

//#define UNUSED(val) (void)(val)
#define TCHAR   char
#define _TCHAR  char
#define _T(str) str
#define scanf_s scanf

// Not sure if I need this later - as such, I (decryptor app) will only ever need to talk to 1 enclave at a time - verifier enclave first and then the apache enclave.
//extern std::map<sgx_enclave_id_t, uint32_t>g_enclave_id_map;
//int __ImageBase=0;

sgx_enclave_id_t enclave_id = 0;
#define Decryptor_PATH "libDecryptor.so"
//////////////////////////////////////////////////

#include <stdio.h>


uint32_t write_to_fd(int fd, uint8_t* msg, uint32_t* expected_msg_length)
{
  lseek(fd, 0, SEEK_SET);
  ssize_t bytes_written;
  bytes_written = write(fd, msg, *expected_msg_length);
  if(bytes_written <= 0)
    return 0xFFFFFFFF;

  fsync(fd);
  *expected_msg_length = bytes_written;
  return 0;
}

uint32_t read_from_fd(int fd, uint8_t* msg, uint32_t* expected_msg_length)
{
  ssize_t bytes_read;
  lseek(fd, 0, SEEK_SET);
  bytes_read = read(fd, msg, *expected_msg_length);
  if(bytes_read <= 0)
    return 0xFFFFFFFF;
  *expected_msg_length = bytes_read;
  return 0;
}


uint32_t unseal_signing_key_pair_from_disk(int fd, size_t sealed_msg_length_in_file)
{
  uint32_t ret_status=0, length=sealed_msg_length_in_file, counter=0;
  uint8_t* sealed_data;

  sealed_data = (uint8_t*) malloc(0x300); //TODO: Get length of the sealed msg and try to read that much from the file.
	// May be pass the length of the file as input to this function and check that it is at least as much as the output of the sgx call.
  ret_status = read_from_fd(fd, sealed_data, &length);
  if(ret_status != 0)
  {
    free(sealed_data);
    return 0xFFFFFFFF;
  }

  for(counter=0;counter<length;counter++)
	printf("%x ", *(sealed_data+counter));
  printf("\n");  fflush(stdout);

  Decryptor_unseal_and_restore_long_term_signing_key_pair_wrapper(enclave_id, &ret_status, sealed_data, &length);
  free(sealed_data);
  return ret_status;
}

uint32_t create_and_seal_signing_key_pair_to_disk(int fd)
{
  uint32_t ret_status=0, length=0, counter=0;
  uint8_t* sealed_data;

  Decryptor_calculate_sealed_keypair_size_wrapper(enclave_id, &length);
  if(length == 0xFFFFFFFF)
    return 0xFFFFFFFF;
  sealed_data=(uint8_t*) malloc(length); // 0x300); // TODO: Shouldn't it be malloc of length?
        printf("length: %d\n", length); fflush(stdout);

  Decryptor_create_and_seal_long_term_signing_key_pair_wrapper(enclave_id, &ret_status, &length, sealed_data);
  if(ret_status != SGX_SUCCESS)
  {
    printf("create_and_seal called returned an error: %x", ret_status);
    free(sealed_data);
    return 0xFFFFFFFF;
  }
  printf("It returned sgx_success\n"); fflush(stdout);
  for(counter=0; counter<length; counter++)
	printf("%02x ", sealed_data[counter]);
  ret_status = write_to_fd(fd, sealed_data, &length);
  free(sealed_data);

  return ret_status;
}

int main(__attribute__((unused)) int argc, __attribute__((unused)) char* argv[])
{
    uint32_t ret_status, counter;
    sgx_status_t status;
    // For sgx setup
    int launch_token_updated;
    sgx_launch_token_t launch_token;

    status = sgx_create_enclave(Decryptor_PATH, SGX_DEBUG_FLAG, &launch_token, &launch_token_updated, &enclave_id, NULL);
    if(status != SGX_SUCCESS)
    {
        printf("\n%02x-Load Enclave Failure", status);
        return -1;
    }
    printf("\nDecryptor - EnclaveID %" PRIx64, enclave_id);
    fflush(stdout);

    // Sealing class looks for this file on disk.
    // If it exists, it reads the file, does an ecall to unseal the content and loads the signing-verification keypair.
    // Otherwise, it runs an ecall that creates such a keypair, seals it and then writes the content to disk.
    // In either case, this keypair is initialized within the enclave.
    Sealing sealer;
    const char* sealed_keypair_file = "sealed_signing_key.txt";
    ret_status = sealer.initialize_long_term_keypair(enclave_id, sealed_keypair_file);
    if(ret_status != 0)
    {
        sgx_destroy_enclave(enclave_id);
        return ret_status;
    }

    // Retrieves the long-term *verification* key that is initialized in the enclave as a result of the previous call.
    uint8_t verification_key[64];
	Decryptor_get_long_term_verification_key_wrapper(enclave_id, verification_key);
	printf("Verification key\n"); fflush(stdout);
	for(counter=0;counter<32;counter++)
		printf("%02x", verification_key[counter]);
	printf("\n"); fflush(stdout);
	for(counter=0;counter<32;counter++)
	    printf("%02x", verification_key[counter + 32]);
	printf("\n"); fflush(stdout);

    MainLogic mainLogic;
    mainLogic.set_enclave_id(enclave_id);
    mainLogic.run_deployment_runtime_stage_logic();

    sgx_destroy_enclave(enclave_id);

    return 0;
}

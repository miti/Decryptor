//
// Created by miti on 2019-12-21.
//

#include "../Include/Sealing.h"

uint32_t Sealing::write_to_fd(int fd, uint8_t* msg, uint32_t* expected_msg_length)
{
    lseek(fd, 0, SEEK_SET);
    ssize_t bytes_written;
    bytes_written = write(fd, msg, *expected_msg_length);
    if(bytes_written <= 0)
        return 0xFFFFFFFF;

    fsync(fd);
    *expected_msg_length = bytes_written;
    close(fd);
    return 0;
}

uint32_t Sealing::read_from_fd(int fd, uint8_t* msg, uint32_t* expected_msg_length)
{
    ssize_t bytes_read;
    lseek(fd, 0, SEEK_SET);
    bytes_read = read(fd, msg, *expected_msg_length);
    if(bytes_read <= 0)
        return 0xFFFFFFFF;
    *expected_msg_length = bytes_read;
    return 0;
}


uint32_t Sealing::unseal_signing_key_pair_from_disk(sgx_enclave_id_t enclave_id, int fd, size_t sealed_msg_length_in_file)
{
    uint32_t ret_status=0, length=sealed_msg_length_in_file, counter=0;
    uint8_t* sealed_data;

    sealed_data = (uint8_t*) malloc(0x300); //TODO: Get length of the sealed msg and try to read that much from the file.
    // May be pass the length of the file as input to this function and check that it is at least as much as the output of the sgx call.
    ret_status = read_from_fd(fd, sealed_data, &length);
    if(ret_status != 0)
    {
        free(sealed_data);
        return 0xFFFFFFFF;
    }

    for(counter=0;counter<length;counter++)
        printf("%x ", *(sealed_data+counter));
    printf("\n");  fflush(stdout);

    Decryptor_unseal_and_restore_long_term_signing_key_pair_wrapper(enclave_id, &ret_status, sealed_data, &length);
    free(sealed_data);
    return ret_status;
}

uint32_t Sealing::create_and_seal_signing_key_pair_to_disk(sgx_enclave_id_t enclave_id, int fd)
{
    uint32_t ret_status=0, length=0, counter=0;
    uint8_t* sealed_data;

    Decryptor_calculate_sealed_keypair_size_wrapper(enclave_id, &length);
    if(length == 0xFFFFFFFF)
        return 0xFFFFFFFF;
    sealed_data=(uint8_t*) malloc(length);
    printf("length: %d\n", length); fflush(stdout);

    Decryptor_create_and_seal_long_term_signing_key_pair_wrapper(enclave_id, &ret_status, &length, sealed_data);
    if(ret_status != SGX_SUCCESS)
    {
        printf("create_and_seal called returned an error: %x", ret_status);
        free(sealed_data);
        return 0xFFFFFFFF;
    }
    printf("It returned sgx_success\n"); fflush(stdout);
    for(counter=0; counter<length; counter++)
        printf("%02x ", sealed_data[counter]);
    ret_status = write_to_fd(fd, sealed_data, &length);
    free(sealed_data);

    return ret_status;
}

// "sealed_signing_key.txt"
uint32_t Sealing::initialize_long_term_keypair(sgx_enclave_id_t enclaveId, const char* filename)
{
    struct stat st;
    size_t sealed_msg_length_in_file;
    int sealed_signing_key_fd;
    uint32_t ret_status;

    sealed_signing_key_fd = open(filename, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    if(sealed_signing_key_fd == -1)
    {
        perror("\nError in opening the file \n");
        fflush(stderr);
        return 0xFFFFFFFF;
    }

    printf("\nSuccessfully opened a file to seal the signing key pair for the client.\n");
    fflush(stdout);

    ret_status = fstat(sealed_signing_key_fd, &st);
    if(ret_status != 0)
    {
        perror("error in finding the file size. -  ");
        fflush(stderr);
        return 0xffffffff;

    }
    sealed_msg_length_in_file = st.st_size;
    if(sealed_msg_length_in_file == 0)
    {
        printf("Creating new keypair.\n"); fflush(stdout);
        ret_status = create_and_seal_signing_key_pair_to_disk(enclaveId, sealed_signing_key_fd);
    }
    else
    {
        printf("Unsealing keypair.\n"); fflush(stdout);
        ret_status = unseal_signing_key_pair_from_disk(enclaveId, sealed_signing_key_fd, sealed_msg_length_in_file);
    }
    return 0;
}

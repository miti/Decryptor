//
// Created by miti on 2020-01-01.
//

#include "MainLogic.h"
#include <stdio.h>

// For socket programming
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

int MainLogic::accept_fd(int fd)
{
    struct sockaddr_storage apache_addr;
    socklen_t apache_addr_size = sizeof(apache_addr);

    return  accept(fd, (struct sockaddr *)&apache_addr,&apache_addr_size);
}

int MainLogic::la_post_la_with_verifier()
{
    int listen_fd, verifier_fd, la_ret, post_la_ret;
    // DEPLOYMENT STAGE.
    listen_fd = set_up_listening_socket(3824);
    if(listen_fd <=0)
    {
        printf("Error in setting up server socket."); fflush(stdout);
        return 0x1;
    }
    printf("Successfully set up a socket to communicate with the verifier enclave.\n");
    fflush(stdout);

    verifier_fd = accept_fd(listen_fd);
    if (verifier_fd <0)
    {
        printf("Error in accepting %d", errno); fflush(stdout);
    }

    la_ret = laInitiator.conduct_la(verifier_fd);
    if(la_ret !=0)
    {
        printf("local attestation - with the verifier - did not successfully return: %x\n", la_ret); fflush(stdout);
        return 0xFFFFFFFF;
    }
    printf("Successfully conducted LA with the verifier.\n"); fflush(stdout);

    messaging.set_verifier_fd(verifier_fd);
    post_la_ret = messaging.receive_token_from_verifier();
    if(post_la_ret != 0)
    {
        printf("Error in post-verifier LA: %ud\n", post_la_ret); fflush(stdout);
        return post_la_ret;
    }
    printf("Obtained the token from the verifier.\n"); fflush(stdout);
    return 0;
}

int MainLogic::la_post_la_with_target()
{
    int listen_fd, la_ret, target_data_fd, target_headers_fd;
    listen_fd = set_up_listening_socket(3825);
    if(listen_fd <=0)
    {
        printf("Error in setting up server socket."); fflush(stdout);
        return listen_fd;
    }
    printf("Successfully set up a socket to communicate with the target enclave.\n");
    fflush(stdout);

    target_data_fd = accept_fd(listen_fd);
    if (target_data_fd <0)
    {
        printf("Error in accepting %d", errno); fflush(stdout);
    }

    la_ret = laInitiator.conduct_la(target_data_fd);
    if(la_ret !=0)
    {
        printf("local attestation - with the target - did not successfully return: %x\n", la_ret); fflush(stdout);
        return 0xFFFFFFFF;
    }
    printf("Successfully conducted LA with the target.\n"); fflush(stdout);

    // For post-la messaging of client data.
    messaging.set_target_data_fd(target_data_fd);

    listen_fd = set_up_listening_socket(3826);
    if(listen_fd <=0)
    {
        printf("Error in setting up server socket for the header.\n"); fflush(stdout);
        return listen_fd;
    }
    printf("Successfully set up a socket to send a header to the target.\n");
    fflush(stdout);

    target_headers_fd = accept_fd(listen_fd);
    if (target_headers_fd <0)
    {
        printf("Error in accepting %d\n", errno); fflush(stdout);
    }
    messaging.set_target_headers_fd(target_headers_fd);

    if(messaging.read_and_write_header() != 0)
    {
        printf("Could not write the header.\n");
        fflush(stdout);
        return 0x1;
    }
    printf("Wrote the header to the target fd for headers.\n"); fflush(stdout);
    return 0;
}

// Sets up a socket to bind and listen to the given port. Returns FD of the socket on success, -1 on failure (and prints a msg to stdout with the errno)
int MainLogic::set_up_listening_socket(int port)
{
    struct sockaddr_in own_addr;
    sockaddr_in* address = &own_addr;
    int server_fd = 0;

    // Creating socket file descriptor for listening for attestation requests.
    server_fd = socket(AF_INET, SOCK_STREAM | SOCK_CLOEXEC, 0);
    if (server_fd == -1)
    {
        printf("Error in creating a socket - %d", errno);
        return -1;
    }

    // Preparing the address struct for binding
    address->sin_family = AF_INET;
    address->sin_addr.s_addr = INADDR_ANY; // Todo: should this be localhost?
    address->sin_port = htons(port);

    socklen_t addrlen = sizeof(*address);
    // Binding
    if (bind(server_fd, (sockaddr*)address, addrlen)<0)
    {
        printf("Error in binding %d - port was %d - ", errno, port);
        return -1;
    }

    // Listening
    if (listen(server_fd, 128) < 0)
    {
        printf("Error in listening %d", errno);
        return -1;
    }

    return server_fd;
}

void MainLogic::set_enclave_id(uint32_t given_enclave_id) {
    laInitiator.set_enclave_id(given_enclave_id);
    messaging.set_enclave_id(given_enclave_id);
}

int MainLogic::run_deployment_runtime_stage_logic() {
    int ret_status;
    pid_t child_pid;

    ret_status = la_post_la_with_verifier();
    if(ret_status != 0)
        return ret_status;

    ret_status = la_post_la_with_target();
    if(ret_status != 0)
        return ret_status;

    child_pid = fork();
    if(child_pid < 0)
    {
        printf("Error forking (to create a process to listen for header requests).\n"); fflush(stdout);
        return 0x1;
    }
    else if(child_pid == 0) // Child process - returns headers.
    {
        printf("Lol in child process.\n"); fflush(stdout);
        ret_status = 0;
        do
        {
            ret_status = messaging.read_and_write_header();
        }    while(ret_status == 0);
        return ret_status;
    }
    else // Parent process - decrypts client data.
    {
        printf("Lol in parent process.\n"); fflush(stdout);
        ret_status = 0;
        do
        {
            ret_status = messaging.receive_decrypt_client_data_from_target();
        }    while(ret_status == 0);
        return ret_status;
    }
}


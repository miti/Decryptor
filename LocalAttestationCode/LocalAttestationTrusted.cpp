/*
 * Copyright (C) 2011-2017 Intel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Intel Corporation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

// #include <stdint.h>
#include "LocalAttestationTrusted.h"
#include "Decryptor.h"
#include "error_codes.h"
#include "datatypes.h"
#include "sgx_tcrypto.h"
#include "sgx_dh.h"

      dh_session_t LocalAttestationTrusted::global_session_info;


//Handle the request from Source Enclave for a session
uint32_t LocalAttestationTrusted::session_request(sgx_dh_msg1_t *dh_msg1)
{
        sgx_dh_session_t sgx_dh_session;
        sgx_status_t status = SGX_SUCCESS;

        //Intialize the session as a session responder
        status = sgx_dh_init_session(SGX_DH_SESSION_RESPONDER, &sgx_dh_session);
        if(SGX_SUCCESS != status)
        {
            return status;
        }

        global_session_info.status = IN_PROGRESS;

        //Generate Message1 that will be returned to Source Enclave
        status = sgx_dh_responder_gen_msg1((sgx_dh_msg1_t*)dh_msg1, &sgx_dh_session);
        if(SGX_SUCCESS != status)
          return status;

        memcpy(&global_session_info.in_progress.dh_session, &sgx_dh_session, sizeof(sgx_dh_session_t));

        return 0;
    }

// TODO: Hope to edit the sgx_dh_responder_proc_msg2 call to return 32 byte key.
//Verify Message 2, generate Message3 and exchange Message 3 with Source Enclave
uint32_t LocalAttestationTrusted::exchange_report(sgx_dh_msg2_t *dh_msg2, sgx_dh_msg3_t *dh_msg3)
{
        sgx_key_128bit_t dh_aek;
        uint32_t status = 0;
        sgx_dh_session_t sgx_dh_session;
        sgx_dh_session_enclave_identity_t initiator_identity;
        uint32_t verify_return;
        memset(&dh_aek,0, sizeof(sgx_key_128bit_t));

        if(global_session_info.status != IN_PROGRESS)
          return INVALID_SESSION;

        memcpy(&sgx_dh_session, &global_session_info.in_progress.dh_session, sizeof(sgx_dh_session_t));

        dh_msg3->msg3_body.additional_prop_length = 0;

        //Process message 2 from source enclave and obtain message 3
        status = sgx_dh_responder_proc_msg2(dh_msg2, dh_msg3, &sgx_dh_session, &dh_aek, &initiator_identity);
        if(SGX_SUCCESS != status)
          return status;

        //Verify source enclave's trust
    	verify_return = Decryptor::verify_peer_enclave_trust(initiator_identity.mr_enclave.m, initiator_identity.mr_signer.m, dh_aek);
        if(verify_return != 0)
         return verify_return;

        return 0;
    }

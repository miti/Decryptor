#include "LocalAttestationCode_t.h"
#include "LocalAttestationTrusted.h"
uint32_t session_request_wrapper(sgx_dh_msg1_t *dh_msg1)
{
  return LocalAttestationTrusted::session_request(dh_msg1);
}

uint32_t exchange_report_wrapper(sgx_dh_msg2_t *dh_msg2, sgx_dh_msg3_t *dh_msg3)
{
  return LocalAttestationTrusted::exchange_report(dh_msg2, dh_msg3);
}
